<?php
require '../vendor/autoload.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header('Content-Type: application/json');
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
$params=json_decode(file_get_contents('php://input'),true);
if(!isset($params['appointment_type'])){
  return http_response_code(400);
}
try{
$client = new \GuzzleHttp\Client();
$response = $client->post('http://staging.smithandnoble.com/rest/V1/appointment/questions', [
  'debug' => false,
  'json' => ['appointment_type'=>$params['appointment_type']],
  'headers' => [
    'Content-Type' => 'application/json',
    'Authorization' => 'Bearer joa99hjt9ussqozcr1aaahrvtmnk2bt6'
  ]
]);

}catch(RequestException $e){
    if ($e->hasResponse()) {
        echo Psr7\str($e->getResponse());
    }
    return http_response_code(400);
}catch(\Exception $e){
	return http_response_code(500);
}
$code=json_decode($response->getBody(), true)['code'];
if(empty($code)){
    print (json_encode(['msg'=>'Bad response']));
    return http_response_code(400);
}
if($code==200){
  print($response->getBody());
  return http_response_code(200);
}else{
  print($response->getBody());
  return http_response_code($code);
}
?>