<?php
require '../vendor/autoload.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");

header("Access-Control-Allow-Headers: Origin");
header('Content-Type: application/json');

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
$params=json_decode(file_get_contents('php://input'),true);


if(!isset($params['zipcode'])){
	return http_response_code(400);
}
try{
$client = new \GuzzleHttp\Client();
$response = $client->post('http://staging.smithandnoble.com/rest/V1/appointment/city-state', [
  'debug' => false,
  'json' => ['zipcode'=>$params['zipcode']],
  'headers' => [
    'Content-Type' => 'application/json',
    'Authorization' => 'Bearer joa99hjt9ussqozcr1aaahrvtmnk2bt6'
  ]
]);
}catch(RequestException $e){
    if ($e->hasResponse()) {
        echo Psr7\str($e->getResponse());
    }
    return http_response_code(400);
}catch(\Exception $e){
	return http_response_code(500);
}
// print($response->getBody());
$code=json_decode($response->getBody(), true);
// print($code['ZipCode']['Zip5']);exit;
if(empty($code['ZipCode']['Zip5'])){
    print (json_encode(['msg'=>'Bad response']));
    return http_response_code(400);
}
if($code==200){
  print($response->getBody());
  return http_response_code(200);
}else{
  print($response->getBody());
  return http_response_code($code);
}

?>