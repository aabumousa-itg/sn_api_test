## LOWES API

### API Endpoints

- alias : redirects the calls to epic aliases
- states : gets list of available states
- questions : fetches questions.
- check-service : checks service depends on zip code
- city-state : get city and state from zip code
- update-contact : updaet bronto contact

### Foundation 6 Client

- using foundation 6 styling multi step form.

## Headers 

### Authorization key :

`Bearer joa99hjt9ussqozcr1aaahrvtmnk2bt6` 

#### How to create Authorization key in Magento ?

1. Login to admin panel
2. System -> Integrations
3. Add new integration and fill the fields 
4. Enable Appointment api endpoints then save
5. Navigate to Integrations then click activate besides your integration
6.  Click Allow
7. Copy the Access Token then click done
8. You can find the access token when you navigate into edit the new integration entity.

### Content type : 

`application/json`

### URL

#### **Magento** {magento site}/rest/V1/appointment/{endpoint}

#### **PHP Client** {phpclient site}/endpoints/{endpoint}.php 

[^you don't need to pass the authorization key to the remote backend client since it acts as a median between Magento API and your frontend client]: 



## API calls flow



check-service -> city-state -> states -> alias["appointment_duration_values"] -> alias ["save_appointment"] -> alias ["appointment_available_dates"] -> alias["appointment_available_times"] -> alias["book_appointment"] -> update-contact -> alias["appointment_assigned_resources"] -> alias["create_appointment_order"] -> questions -> alias ["save_appointment"]

```mermaid

graph TD;
  check-service-->city-state;
  city-state --> states;
  states-->alias__appointment_duration_values;
  alias__appointment_duration_values-->alias__save_appointment ;
  alias__save_appointment-->alias__appointment_available_dates;
  alias__appointment_available_dates-->alias__appointment_available_times;
  alias__appointment_available_times-->alias__book_appointment;
  alias__book_appointment-->update-contact;
  update-contact-->alias__appointment_assigned_resources;
  alias__appointment_assigned_resources-->alias__create_appointment_order;
  alias__create_appointment_order-->questions;
  questions-->alias___save_appointment;
```



# Endpoints

## check-service | POST

parameters : zip code -> 5 digits number

example : `{"zipcode":"92881"}`

responds with available services for zipcode area 

### possible responds

- No coverage record for the specified Zip
- services -> IHD
- services -> MSR
- services -> IHD , MSR

`{"status":1,"code":200,"results":{"services":["IHD","MSR"],"coverage":"D"}}`

## city-state | POST

parameters : zip code -> 5 digits number

example : `{"zipcode":"92881"}`

responds with  city and state related to zip code

### possible responds

- Zip5
- City
- State

`{"ZipCode":{"@attributes":{"ID":"0"},"Zip5":"92881","City":"CORONA","State":"CA"}}`

## states | GET

responds with US states information 

example response:

`{"status":1,"code":200,"results":[{"code":"AL","name":"Alabama"},{"code":"AK","name":"Alaska"},{"code":"AZ","name":"Arizona"},{"code":"AR","name":"Arkansas"},{"code":"CA","name":"California"},{"code":"CO","name":"Colorado"},{"code":"CT","name":"Connecticut"},{"code":"DC","name":"D.C."},{"code":"DE","name":"Delaware"},{"code":"FL","name":"Florida"},{"code":"GA","name":"Georgia"},{"code":"HI","name":"Hawaii"},{"code":"ID","name":"Idaho"},{"code":"IL","name":"Illinois"},{"code":"IN","name":"Indiana"},{"code":"IA","name":"Iowa"},{"code":"KS","name":"Kansas"},{"code":"KY","name":"Kentucky"},{"code":"LA","name":"Louisiana"},{"code":"ME","name":"Maine"},{"code":"MD","name":"Maryland"},{"code":"MA","name":"Massachusetts"},{"code":"MI","name":"Michigan"},{"code":"MN","name":"Minnesota"},{"code":"MS","name":"Mississippi"},{"code":"MO","name":"Missouri"},{"code":"MT","name":"Montana"},{"code":"NE","name":"Nebraska"},{"code":"NV","name":"Nevada"},{"code":"NH","name":"New Hampshire"},{"code":"NJ","name":"New Jersey"},{"code":"NM","name":"New Mexico"},{"code":"NY","name":"New York"},{"code":"NC","name":"North Carolina"},{"code":"ND","name":"North Dakota"},{"code":"OH","name":"Ohio"},{"code":"OK","name":"Oklahoma"},{"code":"OR","name":"Oregon"},{"code":"PA","name":"Pennsylvania"},{"code":"RI","name":"Rhode Island"},{"code":"SC","name":"South Carolina"},{"code":"SD","name":"South Dakota"},{"code":"TN","name":"Tennessee"},{"code":"TX","name":"Texas"},{"code":"UT","name":"Utah"},{"code":"VT","name":"Vermont"},{"code":"VA","name":"Virginia"},{"code":"WA","name":"Washington"},{"code":"WV","name":"West Virginia"},{"code":"WI","name":"Wisconsin"},{"code":"WY","name":"Wyoming"}]}`

## questions | POST

parameters : appointment_type 

- {"appointment_type":"IHD"}

  {"appointment_type":"MSR"}

  `{"appointment_type":"MSR"}`

responds with a list of related questions as a json format.

`{"status":1,"code":200,"results":{"questions_list":{"5":{"Question":"COMMENTS","Seq":"10","Used":"Y","QuestionText":"Additional Comments","Required":"N","FromValue":"0.0000","ToValue":"0.0000","DecPlaces":"1","CodeType":"A","DefaultCode":"1","CodeDisplayType":"R","Cols":"70","Rows":"4","DefaultValue":"","XmlTagCode":"","XmlTagCTbl":"","BillImmediately":"N","MiscProduct":"0","Formula":"","IsQualifier":"N","TS_apptquestion":"2016-08-22 12:59:55","TS_apptquestion_archived":"0000-00-00 00:00:00","AppointmentType":"MSR","Choices":[],"Answer":""}}}}`

## alias | POST

list of aliases 

- **appointment_duration_values** 

  - **appointment_type** : [MSR, IHD]

    `{"api_alias":"appointment_duration_values","appointment_type":"MSR"}`

  - response : number of windows objects

    `{"status":1,"code":200,"results":{"0":{"Rule":"MEASURE","RuleResource":"INSTALLER","Seq":"1","ValueDescription":"1 TO 8 WINDOWS","ValueMin":"1","ValueMax":"8","TimeRequired":"30","Status":"A"},"1":{"Rule":"MEASURE","RuleResource":"INSTALLER","Seq":"1","ValueDescription":"9 TO 15 WINDOWS","ValueMin":"9","ValueMax":"15","TimeRequired":"60","Status":"A"},"2":{"Rule":"MEASURE","RuleResource":"INSTALLER","Seq":"1","ValueDescription":"16 TO 24 WINDOWS","ValueMin":"16","ValueMax":"24","TimeRequired":"60","Status":"A"},"3":{"Rule":"MEASURE","RuleResource":"INSTALLER","Seq":"1","ValueDescription":"24+ WINDOWS","ValueMin":"24","ValueMax":"100","TimeRequired":"90","Status":"A"},"type":"MSR"}}`

- **save_appointment**

  - parameters

    - first_name : string

    - last_name : string

    - email : string : valid email format

    - phone : valid us number 

    - address1 : string

    - address2 : string

    - city : string : auto fill depends on zipcode

    - zip : 5 digits number : auto fill depends on zipcode

    - state : string : auto fill depends on zipcode

    - number_of_windows : integer : ValueMin attribute from appointment_duration_values objects

    - appointment_type : [IHD, MSR] : depends on the first selection of appointment type 

    - time_preference : string : default value "AT"

    - booking_origin : string : default value : "C"

    - phone_code : string : default value : "Web"

    - section : string : default value : "all"

    - duration_value : integer : TimeRequired attribute from appointment_duration_values objects

    - return_appointment : integer : default value 1

    - AnswersSHUTTERS : string: default value "on"

    - AnswersOVER10FT :string :default value "on"

    - api_alias : "save_appointment"

      `{"first_name":"Ibra","last_name":"Yas","email":"iya@it.com","phone":"0597410077","address1":"itg","address2":"","city":"Nablus","zip":"92881","state":"TN","number_of_windows":"9","appointment_type":"MSR","time_preference":"AT","booking_origin":"C","phone_code":"Web","section":"all","duration_value":"1","return_appointment":"1","AnswersSHUTTERS":"on","AnswersOVER10FT":"on","api_alias":"save_appointment"}`

  - response

    - response format

      `{"status":1,"code":200,"results":{"status":true,"appointmentId":7368,"appointment":{"Appointment":"7368","Status":"100","Customer":"","PriorAppointment":"","MeasureAppointment":"","ProductOrder":"0","InstallOrder":"0","MeasureOrder":"0","AppointmentType":"MSR","DurationValue":"1","Name":"Yas","FirstName":"Ibra","Address1":"itg","Address2":"","City":"Nablus","State":"TN","Zip":"92881","ApptAVS":"N","Phone":"(059) 741-0077","Cell":"","AllowText":"","Email":"iya@it.com","TimePreference":"AT","StartTime":"0000-00-00 00:00:00","EndTime":"0000-00-00 00:00:00","TimeRequired":"30","BookingOrigin":"C","Source":"I","MarketingCode":"Z138801238","HearSource":"0","PhoneCode":"Web","HoursRequired":0.5}}}`

- **appointment_available_dates**

  - parameters

    - appointment_id : integer : appointmentId from save_appointment response

    - range_start : current date yyyy-mm-dd

    - range_end : latest date in month : yyyy-mm-dd

    - time_preference : "AT"

    - api_alias : appointment_available_dates

      `{"api_alias":"appointment_available_dates","appointment_id":7368,"range_start":"2019-07-28","range_end":"2019-07-31","time_preference":"AT"}`

  - response

    - responds with the upcoming available dates 

      `{"status":1,"code":200,"results":{"General":[],"Preferred":["2019-07-29","2019-07-30","2019-07-31"]}}`

- **appointment_available_times**

  - parameters

    - appointment_id : integer : appointmentId from save_appointment response

    - start_time : selected date : yyyy-mm-dd

    - api_alias : appointment_available_times

      example : 

      `{"api_alias":"appointment_available_times","appointment_id":7368,"start_time":"2019-07-29"}`

  - response 

    - responds with the upcoming available times

      `{"status":1,"code":200,"results":["2019-07-29T06:30:00","2019-07-29T07:00:00","2019-07-29T10:00:00","2019-07-29T12:00:00","2019-07-29T14:00:00","2019-07-29T14:30:00","2019-07-29T18:00:00","2019-07-29T18:30:00","2019-07-29T19:00:00"]}`

- **book_appointment**

  - parameters

    - api_alias : book_appointment

    - appointment_id : integer : appointmentId from save_appointment response

    - start_time : selected date : yyyy-mm-ddThh-mm-ss

      `{"api_alias":"book_appointment","appointment_id":7368,"start_time":"2019-07-29T19:00:00"}`

  - response

    - status

      `{"status":1,"code":200}`

- **appointment_assigned_resources**

  - parameters 

    - api_alias : appointment_assigned_resources

    - appointment_id : integer : appointmentId from save_appointment response

      `{"api_alias":"appointment_assigned_resources","appointment_id":7368}`

  - response

    - responds with designer and installer

      `{"status":1,"code":200,"results":{"secondaries":[],"primary":{"Appointment":"7368","RuleResource":"INSTALLER","Resource":"100034","StartTime":"2019-07-29 21:00:00","EndTime":"2019-07-29 21:30:00","TimeRequired":"30","Name":"VLAD","FirstName":"PANIBRATETES","Territory":"US","Address1":"5850 ROSEBUD LANE STE A","Address2":"","City":"SACRAMENTO","State":"CA","Zip":"95841","Phone":"(916) 344-3430","HomePhone":"","CellPhone":"(916) 344-3430","AllowText":"N","Fax":"","Email":"","Date":"0000-00-00","DeActivate":"N","CorporateID":"","ResourceType":"E","Vendor":"W9PDS","ResourceTeam":"PROFESSIONAL DRAPERY","PerformanceAllocationRanking":"10.00","DualResourceRequired":"N","NotificationMethod":"","NotifyConfirmation":"N","NotifyReminder":"N","NotifyCancellation":"N","Country":"US","Profile":"Professional Drapery Service, Inc"}}}` 

- **create_appointment_order**

  - parameters

    - api_alias : create_appointment_order

    - appointment_id : integer : appointmentId from save_appointment response

      `{"api_alias":"create_appointment_order","appointment_id":7368}`

  - response

    - responds with appointment confirmation 

      `{"status":1,"code":200,"results":{"success":true,"appointment_id":"7368","measure_order":"20516159"}}`

      

    

- **save_appointment**

  - parameters

    - api_alias : save_appointment

    - appointment_id : integer : appointmentId from save_appointment response

    - section : string : default value "qst"

    - AnswersCOMMENTS : string : comments field value 

      `{"api_alias":"save_appointment","appointment_id":7368,"section":"qst","AnswersCOMMENTS":"thanks"}`

  - response

    - responds with appointment confirmation

      `{"status":1,"code":200,"results":{"status":true,"appointmentId":"7368"}}`

